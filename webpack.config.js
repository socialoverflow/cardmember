const path = require('path');
const HWP = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
        filename: 'build.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
        globalObject: 'this'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /asset\/css\/.*\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /asset\/image\/.*\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'image-webpack-loader',
                        options:
                            {
                                pngquant: {
                                    quality: 80,
                                },
                            },
                    },
                ],
            }
        ]
    },
    plugins:
        [
            new HWP(
                {template: path.resolve(__dirname, 'public/index.html')}
            ),
            new CopyWebpackPlugin([
                {from: 'node_modules/pdfjs-dist/cmaps/', to: 'cmaps/'},
            ]),
            new Dotenv()
        ],
    devServer: {
        historyApiFallback: true,
        overlay: true,
        port: 8000
    }
};
