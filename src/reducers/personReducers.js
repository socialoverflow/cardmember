import {CREATE_PERSON, CREATE_PERSON_FAILED, CREATE_PERSON_SUCCESS, RESET_PERSON_ERROR} from "../actions/personActions";

const initialState = {
    error: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case CREATE_PERSON:
            return {...state, data: action.payload};
        case CREATE_PERSON_SUCCESS:
            return {...state, data: action.payload.person};
        case CREATE_PERSON_FAILED:
            return {...state, error: action.payload.error};
        case RESET_PERSON_ERROR:
            return {...state, ...initialState};
        default:
            return state;
    }
};
