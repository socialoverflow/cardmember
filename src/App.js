import React from 'react';
import {Main} from "./Main";
import {ConnectedRouter} from "connected-react-router";

export const App = (props) => {
  return (
      <ConnectedRouter history={props.history} context={props.context}>
        <Main/>
      </ConnectedRouter>
  );
};

export default App;
