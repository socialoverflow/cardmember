import 'regenerator-runtime/runtime'
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {rootReducer} from './reducers/rootReducers';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/rootSagas';
import {routerMiddleware} from 'connected-react-router';
import {createBrowserHistory} from 'history';

export const history = createBrowserHistory();

const composeEnhancers = composeWithDevTools({
  // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
    rootReducer(history),
    composeEnhancers(
        applyMiddleware(
            ...[thunk, sagaMiddleware],
            routerMiddleware(history)
        ),
    // other store enhancers if any
  ));

// then run the saga
sagaMiddleware.run(rootSaga);
