import React from 'react';
import PersonCreate from "./Components/Person/PersonCreate";
import PersonEdit from "./Components/Person/PersonEdit";
import {Route, Switch} from 'react-router';
import Header from "./Header";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    app: {
        height: '100vh',
        display: 'flex',
        marginTop: 56,
        [theme.breakpoints.up('sm')]: {
            marginTop: 64,
        },
    },
    main: {
        padding: 0,
        [theme.breakpoints.up('sm')]: {
            padding: 10
        },
        width: '100%'
    }
}));

export const Main = (props) => {
    const classes = useStyles();
    return (
        <div className={classes.app}>
            <Header/>
            <main className={classes.main}>
                <Switch>
                    <Route path='/create' component={PersonCreate}/>
                    <Route path='/edit' component={PersonEdit}/>
                </Switch>
            </main>
        </div>
    )
};
