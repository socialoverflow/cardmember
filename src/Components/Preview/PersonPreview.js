import React from 'react';
import {Document, Image, Page, Text, View, StyleSheet} from "@react-pdf/renderer";
import logo from '../../asset/image/logo_overflow_200.png';
import {memberTypes} from "../Person/PersonForm";

const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
        backgroundColor: '#FFF',
        color: '#C1002A'
    },
    section: {
        margin: 10,
        padding: 1,
    },
    containerRight: {
        flexDirection: 'column'
    },
    containerLeft: {
        flexDirection: 'column',
    },
    logo: {
        margin: '5px 0px 0px -43px',
        height: 100,
    }
});
;

class PersonPreview extends React.Component {
    render() {
        return (
            <Document>
                <Page size={{width: 321.259843, height: 207.87401575}} style={styles.page} wrap>
                    <View style={styles.containerRight}>
                        <View style={styles.section}>
                            <Text>
                                {this.props.person.firstname === undefined ? 'Prénom' : this.props.person.firstname}
                            </Text>
                        </View>
                        <View style={styles.section}>
                            <Text>
                                {this.props.person.lastname === undefined ? "Nom" : this.props.person.lastname}
                            </Text>
                        </View>
                        <View style={styles.section}>
                            <Text>
                                {this.props.person.member_type === undefined ? "Type de membre" : memberTypes[this.props.person.member_type]}
                            </Text>
                        </View>
                        <View style={styles.section}>
                            <Text>
                                XXXXXXXXXXX
                            </Text>
                        </View>
                        <View style={styles.section}>
                            <Text>
                                {`Année : ${this.props.person.year}/${this.props.person.year + 1}`}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.containerLeft}>
                    </View>
                    <Image src={logo} style={styles.logo}/>
                </Page>
            </Document>
        )
    }
}

export default PersonPreview;
