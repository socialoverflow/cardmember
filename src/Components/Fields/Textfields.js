import React from "react";
import TextField from "@material-ui/core/es/TextField/TextField";

export const renderTextField = ({meta: {touched, error} = {}, input: {value, ...inputProps}, ...props}) => {
    return (
      <TextField
        error={!!(touched && error)}
        helperText={touched && error}
        defaultValue={value}
        {...inputProps}
        {...props}
      />
    )
  }
;
