import React from "react";
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from "@material-ui/core/FormHelperText";

export const renderSelectField = ({
                                      input,
                                      label,
                                      meta: {touched, error},
                                      children,
                                      fullWidth,
                                      ...custom
                                  }) => (
    <FormControl fullWidth={fullWidth} error={touched && error}>
        <InputLabel htmlFor={label}>{label}</InputLabel>
        <Select
            autoWidth={fullWidth}
            {...input}
            {...custom}
            inputProps={{
                name: label,
                id: label
            }}
        >
            {children}
        </Select>
        {renderFromHelper({touched, error})}
    </FormControl>
);

const renderFromHelper = ({touched, error}) => {
    if (!(touched && error)) {
        return
    } else {
        return <FormHelperText>{touched && error}</FormHelperText>
    }
};