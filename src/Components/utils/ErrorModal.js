import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import {renderError} from "../../utils/utils";
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

const ErrorModal = ({open, handleClose, error}) => {
    const classes = useStyles();

    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <div className={classes.paper}>
                <h2 id="transition-modal-title">Erreur</h2>
                <p id="transition-modal-description">
                    {
                        error && (renderError(error.status))
                    }
                    {console.log(error)}
                </p>
            </div>
        </Modal>
    );
};

ErrorModal.prototype = {
    error: PropTypes.object,
    handleClose: PropTypes.func,
    open: PropTypes.bool
};

export default ErrorModal;
