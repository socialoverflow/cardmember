import React from 'react';
import Field from "redux-form/es/Field";
import Button from '@material-ui/core/Button';
import {renderTextField} from "../Fields/Textfields";
import {CREATED, renderButton} from "../../utils/utils";
import {reduxForm} from "redux-form";
import {createStyles, withStyles} from "@material-ui/core/styles/index";
import {renderSelectField} from "../Fields/SelectField";
import MenuItem from '@material-ui/core/MenuItem';
import compose from "recompose/compose";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {createPerson, resetPersonError} from "../../actions/personActions";
import ErrorModal from "../utils/ErrorModal";

export const memberTypes = [
    'Président', 'Trésorier', 'Secretaire', 'Membre'
];

const useStyles = createStyles(theme => ({
    button: {
        marginTop: theme.spacing(1)
    }
}));

const validate = values => {
    const errors = {};
    if (!values.firstname) {
        errors.firstname = 'Le prénom est obligatoire'
    }
    if (!values.lastname) {
        errors.lastname = 'Le nom est obligatoire'
    }
    if (!values.email) {
        errors.email = 'L\'email est obligatoire'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Email invalide'
    }
    if (!values.phone_number) {
        errors.phone_number = 'Le n° de téléphone est obligatoire'
    } else if (!/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/i.test(values.phone_number)) {
        errors.phone_number = 'N° de téléphone invalide'
    }

    return errors
};

class PersonForm extends React.Component {

    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {showDialogError: false}
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //show dialog error
        if (prevProps.error !== this.props.error && this.props.error && !this.props.error.errors) {
            console.log('show dialog error');
            this.setState({showDialogError: true});
        }
    }

    handleClose() {
        console.log('handle close');
        this.setState({showDialogError: false});
        //@TODO dispatch action to reset error
        this.props.resetPersonError();
    }


    onSubmit(values) {
        //@TODO call action to dispatch "CREATE PERSON", thing to check type of form
        console.log('create person');
        if (this.props.type === CREATED) {
            this.props.createPerson(
                {
                    firstname: values.firstname,
                    lastname: values.lastname,
                    email: values.email,
                    phoneNumber: values.phone_number,
                    memberType: values.member_type
                }
            )
        }
    }

    render() {

        const {handleSubmit} = this.props;
        return (
            <form onSubmit={handleSubmit(this.onSubmit)}>
                <Field
                    fullWidth
                    name="firstname"
                    component={renderTextField}
                    label="Prénom"
                />
                <Field
                    fullWidth
                    name="lastname"
                    component={renderTextField}
                    label="Nom"
                />
                <Field
                    fullWidth
                    name="email"
                    component={renderTextField}
                    label="Email"
                />
                <Field
                    fullWidth
                    name="member_type"
                    component={renderSelectField}
                    label="Type de membre"
                >
                    <MenuItem value={0}>{memberTypes[0]}</MenuItem>
                    <MenuItem value={1}>{memberTypes[1]}</MenuItem>
                    <MenuItem value={2}>{memberTypes[2]}</MenuItem>
                    <MenuItem value={3}>{memberTypes[3]}</MenuItem>
                </Field>
                <Field
                    fullWidth
                    name="phone_number"
                    component={renderTextField}
                    label="N° de téléphone"
                />
                <div>
                    <Button className={this.props.classes.button} variant="contained" type="submit">
                        {
                            renderButton(this.props.type)
                        }
                    </Button>
                </div>
                <ErrorModal
                    handleClose={this.handleClose}
                    open={this.state.showDialogError}
                    error={this.props.error}
                />
            </form>
        )
    }
}

const enhance = compose(
    reduxForm({
        form: 'person-form',
        validate
    }),
    connect(
        state => ({error: state.person.error}),
        dispatch => ({
            createPerson: bindActionCreators(createPerson, dispatch),
            resetPersonError: bindActionCreators(resetPersonError, dispatch)
        })),
    withStyles(useStyles)
);

export default enhance(PersonForm);