import React from 'react';
import Card from "@material-ui/core/es/Card/Card";
import CardHeader from "@material-ui/core/es/CardHeader/CardHeader";
import CardContent from "@material-ui/core/es/CardContent/CardContent";
import {createStyles, withStyles} from "@material-ui/core/styles/index";
import {EDITED} from "../../utils/utils";
import PersonForm from "./PersonForm";
import PersonPreview from "../Preview/PersonPreview";
import {BlobProvider, PDFDownloadLink} from '@react-pdf/renderer';
import {Document, Page} from 'react-pdf/dist/entry.webpack';
import {formValueSelector} from 'redux-form';
import {connect} from 'react-redux'
import compose from "recompose/compose";

const useStyles = createStyles(theme => ({
    container: {
        display: 'flex',
        [theme.breakpoints.up('sm')]: {
            flexDirection: 'row',
            flexGrow: 1
        },
        flexDirection: 'column',
    },
    form: {
        [theme.breakpoints.up('sm')]: {
            width: '30vw'
        },
    },
    preview: {
        width: 'calc(100vw - 80px)',
        overflowX: 'scroll',
        [theme.breakpoints.up('sm')]: {
            overflowX: 'hidden',
            flex: 'auto',
            alignSelf: 'center',
            flexDirection: 'column',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: 'auto'
        },
    },
    pdfPreview: {
        border: '1px solid #949494',
        width: 323, height: 210,
        margin: 'auto',
    }
}));

const year = new Date().getFullYear();

class PersonCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {displayLink: false}
    }

    render() {
        return (
            <Card>
                <CardHeader title={"Modification d'un nouvelle adhèrent"}/>
                <div className={this.props.classes.container}>
                    <CardContent className={this.props.classes.form}>
                        <PersonForm type={EDITED}/>
                    </CardContent>
                    <CardContent className={this.props.classes.preview}>
                        <BlobProvider document={
                            <PersonPreview
                                person={{
                                    firstname: this.props.firstname,
                                    lastname: this.props.lastname,
                                    member_type: this.props.member_type,
                                    year: year
                                }}
                            />
                        }
                        >
                            {({blob, url, loading, error}) => {
                                return (!loading && (
                                        <Document
                                            options={{
                                                cMapUrl: 'cmaps/',
                                                cMapPacked: true,
                                            }}
                                            renderMode='svg'
                                            file={url}
                                            onLoadSuccess={() => (this.setState({displayLink: true}))}
                                        >
                                            <Page
                                                pageNumber={1}
                                                className={this.props.classes.pdfPreview}
                                            />
                                        </Document>

                                    )
                                )
                            }}
                        </BlobProvider>
                        {this.props.type === EDITED && (
                            <PDFDownloadLink
                                document={<PersonPreview/>}
                            >
                                {({blob, url, loading, error}) => (loading && this.state.displayLink ? 'Chargement…' : 'Télécharger maintenant!')}
                            </PDFDownloadLink>
                        )}
                    </CardContent>
                </div>
            </Card>
        )
    }
}

const selector = formValueSelector('person-form');

const enhance = compose(
    connect(state => ({
        firstname: selector(state, 'firstname'),
        lastname: selector(state, 'lastname'),
        member_type: selector(state, 'member_type'),
    })),
    withStyles(useStyles)
);

export default enhance(PersonCreate);