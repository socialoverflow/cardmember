import {CREATE_PERSON, createPersonFailed, createPersonSuccess} from "../actions/personActions";
import {call, put, takeEvery} from "@redux-saga/core/effects";
import {post} from "../utils/fecthUtils";
import {mapViolationToErrors} from "../utils/utils";
import {stopSubmit} from 'redux-form';
import {push} from 'connected-react-router';

function* postPerson(action) {
    console.log("post person");
    try {
        let response = yield call(post, `/members`, {
            firstname: action.payload.firstname,
            lastname: action.payload.lastname,
            email: action.payload.email,
            phoneNumber: action.payload.phoneNumber,
            memberType: action.payload.memberType
        });
        console.log("success post person");
        yield put(push('/edit'));
        yield put(createPersonSuccess({person: response.body}));
    } catch (e) {
        yield put(stopSubmit('person-form', mapViolationToErrors(e.errors.violations)));
        yield put(createPersonFailed({error: e}))
    }
}

export function* createPerson() {
    yield takeEvery(CREATE_PERSON, postPerson);
}