import {all} from 'redux-saga/effects';
import {createPerson} from './personSagas';

export default function* rootSaga() {
    yield all([createPerson()])
}
