export const CREATE_PERSON = 'CREATE_PERSON';
export const CREATE_PERSON_SUCCESS = 'CREATE_PERSON_SUCCESS';
export const CREATE_PERSON_FAILED = 'CREATE_PERSON_FAILED';
export const RESET_PERSON_ERROR = 'RESET_PERSON_ERROR';

export function createPerson(payload) {
    return {type: CREATE_PERSON, payload};
}

export function createPersonSuccess(payload) {
    return {type: CREATE_PERSON_SUCCESS, payload};
}

export function createPersonFailed(payload) {
    return {type: CREATE_PERSON_FAILED, payload};
}

export function resetPersonError() {
    return {type: RESET_PERSON_ERROR}
}
