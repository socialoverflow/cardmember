export const EDITED = 'EDITED';
export const CREATED = 'CREATED';

export const NOT_FOUND = 404;
export const INTERNAL_SERVER_ERROR = 500;

export const renderButton = (type) => {
    switch (type) {
        case EDITED:
            return "Modifier";
        case CREATED:
            return "Ajouter";
    }
};


export const renderError = (status) => {
    switch (status) {
        case NOT_FOUND:
            return 'Le serveur est injoignable';
        case INTERNAL_SERVER_ERROR:
            return 'Une erreur interne au serveur est survenue';
    }
};

export const mapViolationToErrors = (violations) => {
    let errors = [];
    violations.forEach((violation) => {
        let error = {};
        error[violation.propertyPath] = violation.message;
        errors = {...errors, ...error};
    });
    return errors;
};