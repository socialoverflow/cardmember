export const post = function (endpoint, body) {
    var h = new Headers();
    // const token = localStorage.getItem('token');
    // h.append('Authorization', `Bearer ${token}`);
    h.append('Accept', 'application/json');
    h.append('Content-Type', 'application/json');
    return fetch(process.env.REACT_APP_API_URL + endpoint, {method: 'POST', headers: h, body: JSON.stringify(body)})
        .then(response =>
            response.json().then(json => ({
                status: response.status,
                statusText: response.statusText,
                headers: response.headers,
                body: json,
            }))
        )
        .then(({status, statusText, headers, body}) => {
            let json;
            try {
                json = JSON.parse(body);
            } catch (e) {
                if (status < 200 || status >= 300) {
                    return Promise.reject(
                        {
                            status: status,
                        }
                    );
                }
            }
            if (status < 200 || status >= 300) {
                return Promise.reject(
                    {
                        status: status,
                        errors: json
                    }
                );
            }
            return Promise.resolve({status, headers, body, json});
        });
};